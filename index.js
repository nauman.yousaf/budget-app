const express = require('express'); //import module
// the above constant will return a function called express();
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Budget App API",
      version: "1.0.0",
      description: "A simple budget app API"
    },
    servers: [
      {
        // url: "http://localhost:5000/",
        url: "https://rocky-chamber-02080.herokuapp.com/",
      }
    ]
  },
  apis: ["./routes/*.js"]
};
const specs = swaggerJsDoc(options);
const app = express();
// this express method retrun an object which has a bunch of methods
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

// for parsing application/json
app.use(express.json()); 

app.use(express.urlencoded({ extended: true })); 

const routes  = require("./routes/index");
app.use('/',routes);

// const db = require('./connection.js');

// ########################### Routes #####################################################################

// const userRouter = require("./routes/users");
// app.use('/users', userRouter);
// const incomeRouter = require("./routes/incomes");
// app.use('/incomes', incomeRouter);
// const budgetRouter = require("./routes/budgets");
// app.use('/budgets', budgetRouter);
// const categoriesRouter = require("./routes/categories");
// app.use('/categories', categoriesRouter);
// const expensesRouter = require("./routes/expenses");
// app.use('/expenses', expensesRouter);
// const friendsRouter = require("./routes/friends");
// app.use('/friends', friendsRouter);
// const groupsRouter = require("./routes/groups");
// app.use('/groups', groupsRouter);
// const userGroupsRouter = require("./routes/user_groups");
// app.use('/user-groups', userGroupsRouter);
// const loansRouter = require("./routes/loans");
// app.use('/loans', loansRouter);

// ########################### Routes ########################################################################

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
