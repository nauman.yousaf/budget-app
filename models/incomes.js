'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Incomes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Incomes.init({
    amount: DataTypes.FLOAT,
    source: DataTypes.ENUM('salary', 'business'),
    user_id: DataTypes.INTEGER,
    budget_id: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Incomes',
    tableName:'incomes',
    timestamps:false
  });
  return Incomes;
};
