'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Budgets extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Budgets.init({
    name: DataTypes.STRING,
    planned_expenses: DataTypes.FLOAT,
    planned_incomes: DataTypes.FLOAT,
    start_balance: DataTypes.FLOAT,
    end_balance: DataTypes.FLOAT,
    user_id: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Budgets',
    tableName: 'budgets',
    timestamps: false
  });
  return Budgets;
};
