const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const user_groups = require('../controllers/controller.user_groups');
/**

 * @swagger
 * components:
 *   schemas:
 *      User Groups:
 *       type: object
 *         - user_id
 *         - group_id
 *         - is_owner
 *       properties:  
 *         user_id:
 *           type: integer
 *           description: User id
 *         group_id:
 *           type: integer
 *           description: Group id
 *         is_owner:
 *           type: boolean
 *           description: If user is owner of group
 *       example:
 *          name: 3
 *          group_id: 4
 *          is_owner: true
 * 
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: User Groups
  *   description: The User Groups managing API
  */

/**
 * @swagger
 * /user-groups:
 *   get:
 *     summary: Returns the list of all the User Groups
 *     tags: [User Groups]
 *     responses:
 *       200:
 *         description: The list of the User Groups
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User Groups'
 */

router.get("/", user_groups.getAll);

/**
 * @swagger
 * /user-groups/{id}:
 *   get:
 *     summary: Get the User Groups by id
 *     tags: [User Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The User Groups id
 *     responses:
 *       200:
 *         description: The User Groups description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User Groups'
 *       404:
 *         description: The User Groups was not found
 */

router.get("/:id", user_groups.getRec);

/**
 * @swagger
 * /user-groups:
 *   post:
 *     summary: Create a new User Groups
 *     tags: [User Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User Groups'
 *     responses:
 *       200:
 *         description: The User Groups was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User Groups'
 *       500:
 *         description: Some server error
 */

router.post("/", user_groups.createRec);

/**
 * @swagger
 * /user-groups/{id}:
 *  put:
 *    summary: Update the User Groups by the id
 *    tags: [User Groups]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The User Groups id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User Groups'
 *    responses:
 *      200:
 *        description: The User Groups was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User Groups'
 *      404:
 *        description: The User Groups was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id",user_groups.updateRec);

/**
 * @swagger
 * /user-groups/{id}:
 *   delete:
 *     summary: Remove the User Groups by id
 *     tags: [User Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The User Groups id
 * 
 *     responses:
 *       200:
 *         description: The User Groups was deleted
 *       404:
 *         description: The User Groups was not found
 */

router.delete("/:id", user_groups.deleteRec);

module.exports = router;
