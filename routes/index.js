'use strict';
const fs = require('fs');
const path = require('path');
const router = require('express').Router();
const basename = path.basename(__filename);
fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('route.') == 0) && (file !== basename) && ( (file.slice(-3) === '.js') || (file.slice(-3) === '.ts') );
  })
  .forEach(file => {
    const routeFile = file.split('route.')[1];
    const fileName = routeFile.split('.').slice(0, -1).join('.');
    const nameRouter = require("./"+file);
    router.use('/'+fileName, nameRouter);

  });
module.exports = router;
