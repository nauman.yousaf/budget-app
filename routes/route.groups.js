const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const groups = require('../controllers/controller.groups');
/**

 * @swagger
 * components:
 *   schemas:
 *      Groups:
 *       type: object
 *         - name
 *         - type
 *         - image
 *       properties:  
 *         name:
 *           type: string
 *           description: Name of Group
 *         type:
 *           type: array
 *           items: {['group1', 'group2']}
 *           description: Group type
 *         image:
 *           type: string
 *           description: Group image
 *       example:
 *          name: Friends
 *          type: group1
 *          image: https://picsum.photos/seed/picsum/200/300
 * 
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Groups
  *   description: The Groups managing API
  */

/**
 * @swagger
 * /groups:
 *   get:
 *     summary: Returns the list of all the Groups
 *     tags: [Groups]
 *     responses:
 *       200:
 *         description: The list of the Groups
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Groups'
 */

router.get("/", groups.getAll);

/**
 * @swagger
 * /groups/{id}:
 *   get:
 *     summary: Get the Groups by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Groups id
 *     responses:
 *       200:
 *         description: The Groups description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Groups'
 *       404:
 *         description: The Groups was not found
 */

router.get("/:id", groups.getRec);

/**
 * @swagger
 * /groups:
 *   post:
 *     summary: Create a new Groups
 *     tags: [Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Groups'
 *     responses:
 *       200:
 *         description: The Groups was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Groups'
 *       500:
 *         description: Some server error
 */

router.post("/", groups.createRec);

/**
 * @swagger
 * /groups/{id}:
 *  put:
 *    summary: Update the Groups by the id
 *    tags: [Groups]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Groups id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Groups'
 *    responses:
 *      200:
 *        description: The Groups was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Groups'
 *      404:
 *        description: The Groups was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", groups.updateRec);

/**
 * @swagger
 * /groups/{id}:
 *   delete:
 *     summary: Remove the Groups by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Groups id
 * 
 *     responses:
 *       200:
 *         description: The Groups was deleted
 *       404:
 *         description: The Groups was not found
 */

router.delete("/:id",groups.deleteRec);

module.exports = router;
