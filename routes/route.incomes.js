const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const incomes = require('../controllers/controller.incomes');
/**

 * @swagger
 * components:
 *   schemas:
 *      Incomes:
 *       type: object
 *         - amount
 *         - source 
 *         - user_id
 *         - budget_id 
 *       properties:
 *         amount:
 *           type: number
 *           format: float
 *           description: Amount 
 *         source:
 *           type: array
 *           
 *         user_id:
 *           type: integer
 *           description: User id 
 *         budget_id:
 *           type: integer
 *           description: Budget id
 *       example:
 *         amount: 98.35
 *         source: business
 *         user_id: 3
 *         budget_id: 1
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Incomes
  *   description: The incomes managing API
  */

/**
 * @swagger
 * /incomes:
 *   get:
 *     summary: Returns the list of all the incomes
 *     tags: [Incomes]
 *     responses:
 *       200:
 *         description: The list of the incomes
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Incomes'
 */

router.get("/", incomes.getAll);

/**
 * @swagger
 * /incomes/{id}:
 *   get:
 *     summary: Get the Income by id
 *     tags: [Incomes]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Income id
 *     responses:
 *       200:
 *         description: The Income description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Incomes'
 *       404:
 *         description: The Income was not found
 */

router.get("/:id", incomes.getRec);

/**
 * @swagger
 * /incomes:
 *   post:
 *     summary: Create a new Income
 *     tags: [Incomes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Incomes'
 *     responses:
 *       200:
 *         description: The Income was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Incomes'
 *       500:
 *         description: Some server error
 */

router.post("/", incomes.createRec);

/**
 * @swagger
 * /incomes/{id}:
 *  put:
 *    summary: Update the Income by the id
 *    tags: [Incomes]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Income id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Incomes'
 *    responses:
 *      200:
 *        description: The Income was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Incomes'
 *      404:
 *        description: The Income was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", incomes.updateRec);

/**
 * @swagger
 * /incomes/{id}:
 *   delete:
 *     summary: Remove the Income by id
 *     tags: [Incomes]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Income id
 * 
 *     responses:
 *       200:
 *         description: The Income was deleted
 *       404:
 *         description: The Income was not found
 */

router.delete("/:id", incomes.deleteRec);

module.exports = router;
