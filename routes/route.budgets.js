const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const budget = require('../controllers/controller.budgets');
/**

 * @swagger
 * components:
 *   schemas:
 *      Budgets:
 *       type: object
 *         - name
 *         - planned_expenses
 *         - planned_incomes 
 *         - start_balance 
 *         - end_balance 
 *         - user_id
 *       properties:
 *         name:
 *           type: string
 *           description: Name of expensecc                 
 *         planned_expenses:
 *           type: number
 *           format: float
 *           description: Planned Expense 
 *         planned_incomes:
 *           type: number
 *           format: float
 *           description: Planned Budget 
 *         start_balance:
 *           type: number
 *           format: float
 *           description: Start Banalce 
 *         end_balance:
 *           type: number
 *           format: float
 *           description: End Balance   
 *         user_id:
 *           type: integer
 *           description: User id 
 *         budget_id:
 *           type: integer
 *           description: Budget id
 *       example:
 *          name: "Food"
 *          planned_expenses: 12.80
 *          planned_incomes: 110.00
 *          start_balance: 500.00
 *          end_balance: 2500.00
 *          user_id: 3
 * 
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Budgets
  *   description: The budgets managing API
  */

/**
 * @swagger
 * /budgets:
 *   get:
 *     summary: Returns the list of all the budgets
 *     tags: [Budgets]
 *     responses:
 *       200:
 *         description: The list of the budgets
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Budgets'
 */

router.get("/", budget.getAll);

/**
 * @swagger
 * /budgets/{id}:
 *   get:
 *     summary: Get the Budget by id
 *     tags: [Budgets]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Budget id
 *     responses:
 *       200:
 *         description: The Budget description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Budgets'
 *       404:
 *         description: The Budget was not found
 */

router.get("/:id", budget.getRec);

/**
 * @swagger
 * /budgets:
 *   post:
 *     summary: Create a new Budget
 *     tags: [Budgets]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Budgets'
 *     responses:
 *       200:
 *         description: The Budget was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Budgets'
 *       500:
 *         description: Some server error
 */

router.post("/", budget.createRec);

/**
 * @swagger
 * /budgets/{id}:
 *  put:
 *    summary: Update the Budget by the id
 *    tags: [Budgets]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Budget id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Budgets'
 *    responses:
 *      200:
 *        description: The Budget was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Budgets'
 *      404:
 *        description: The Budget was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", budget.updateRec);

/**
 * @swagger
 * /budgets/{id}:
 *   delete:
 *     summary: Remove the Budget by id
 *     tags: [Budgets]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Budget id
 * 
 *     responses:
 *       200:
 *         description: The Budget was deleted
 *       404:
 *         description: The Budget was not found
 */

router.delete("/:id", budget.deleteRec);

module.exports = router;
