const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const loans = require('../controllers/controller.loans');
/**

 * @swagger
 * components:
 *   schemas:
 *      Loans:
 *       type: object
 *         - amount
 *         - user_id
 *         - budget_id
 *       properties:  
 *         amount:
 *           type: number
 *           description: Amount of loan
 *         user_id:
 *           type: integer
 *           description: User id
 *         budget_id:
 *           type: integer
 *           description: Budget id
 *       example:
 *          amount: 2000
 *          user_id: 3
 *          budget_id: 5
 * 
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Loans
  *   description: The Loans managing API
  */

/**
 * @swagger
 * /loans:
 *   get:
 *     summary: Returns the list of all the Loans
 *     tags: [Loans]
 *     responses:
 *       200:
 *         description: The list of the Loans
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Loans'
 */

router.get("/", loans.getAll);

/**
 * @swagger
 * /loans/{id}:
 *   get:
 *     summary: Get the Loans by id
 *     tags: [Loans]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Loans id
 *     responses:
 *       200:
 *         description: The Loans description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Loans'
 *       404:
 *         description: The Loans was not found
 */

router.get("/:id", loans.getRec);

/**
 * @swagger
 * /loans:
 *   post:
 *     summary: Create a new Loans
 *     tags: [Loans]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Loans'
 *     responses:
 *       200:
 *         description: The Loans was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Loans'
 *       500:
 *         description: Some server error
 */

router.post("/", loans.createRec);

/**
 * @swagger
 * /loans/{id}:
 *  put:
 *    summary: Update the Loans by the id
 *    tags: [Loans]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Loans id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Loans'
 *    responses:
 *      200:
 *        description: The Loans was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Loans'
 *      404:
 *        description: The Loans was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", loans.updateRec);

/**
 * @swagger
 * /loans/{id}:
 *   delete:
 *     summary: Remove the Loans by id
 *     tags: [Loans]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Loans id
 * 
 *     responses:
 *       200:
 *         description: The Loans was deleted
 *       404:
 *         description: The Loans was not found
 */

router.delete("/:id", loans.deleteRec);

module.exports = router;
