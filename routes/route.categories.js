const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const categories = require('../controllers/controller.categories');
/**

 * @swagger
 * components:
 *   schemas:
 *      Categories:
 *       type: object
 *         - name
 *         - parent_category_id
 *         - user_id
 *       properties:
 *         name:
 *           type: string
 *           description: Name of category                   
 *         parent_category_id:
 *           type: integer
 *           description: Parent Category id 
 *         user_id:
 *           type: integer
 *           description: User id 
 *       example:
 *          name: "House Hold"
 *          parent_category_id: 1
 *          user_id: 3
 */

 /**
  * @swagger
  * tags:
  *   name: Categories
  *   description: The categories managing API
  */

/**
 * @swagger
 * /categories:
 *   get:
 *     summary: Returns the list of all the categories
 *     tags: [Categories]
 *     responses:
 *       200:
 *         description: The list of the categories
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Categories'
 */

router.get("/", categories.getAll);

/**
 * @swagger
 * /categories/{id}:
 *   get:
 *     summary: Get the Categories by id
 *     tags: [Categories]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Categories id
 *     responses:
 *       200:
 *         description: The Categories description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Categories'
 *       404:
 *         description: The Categories was not found
 */

router.get("/:id", categories.getRec);

/**
 * @swagger
 * /categories:
 *   post:
 *     summary: Create a new Categories
 *     tags: [Categories]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Categories'
 *     responses:
 *       200:
 *         description: The Categories was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Categories'
 *       500:
 *         description: Some server error
 */

router.post("/", categories.createRec);

/**
 * @swagger
 * /categories/{id}:
 *  put:
 *    summary: Update the Categories by the id
 *    tags: [Categories]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Categories id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Categories'
 *    responses:
 *      200:
 *        description: The Categories was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Categories'
 *      404:
 *        description: The Categories was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", categories.updateRec);

/**
 * @swagger
 * /categories/{id}:
 *   delete:
 *     summary: Remove the Categories by id
 *     tags: [Categories]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Categories id
 * 
 *     responses:
 *       200:
 *         description: The Categories was deleted
 *       404:
 *         description: The Categories was not found
 */

router.delete("/:id",categories.deleteRec);

module.exports = router;
