const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const friends = require('../controllers/controller.friends');
/**

 * @swagger
 * components:
 *   schemas:
 *      Friends:
 *       type: object
 *         - friend_of
 *         - friend_to
 *       properties:  
 *         friend_of:
 *           type: integer
 *           description: Friend of id 
 *         friend_to:
 *           type: integer
 *           description: Friend to id
 *       example:
 *          friend_of: 3
 *          friend_to: 2
 * 
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Friends
  *   description: The Friends managing API
  */

/**
 * @swagger
 * /friends:
 *   get:
 *     summary: Returns the list of all the Friends
 *     tags: [Friends]
 *     responses:
 *       200:
 *         description: The list of the Friends
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Friends'
 */

router.get("/", friends.getAll);

/**
 * @swagger
 * /friends/{id}:
 *   get:
 *     summary: Get the friends by id
 *     tags: [Friends]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The friends id
 *     responses:
 *       200:
 *         description: The friends description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Friends'
 *       404:
 *         description: The friends was not found
 */

router.get("/:id",friends.getRec);

/**
 * @swagger
 * /friends:
 *   post:
 *     summary: Create a new friends
 *     tags: [Friends]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Friends'
 *     responses:
 *       200:
 *         description: The friends was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Friends'
 *       500:
 *         description: Some server error
 */

router.post("/", friends.createRec);

/**
 * @swagger
 * /friends/{id}:
 *  put:
 *    summary: Update the friends by the id
 *    tags: [Friends]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The friends id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Friends'
 *    responses:
 *      200:
 *        description: The friends was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Friends'
 *      404:
 *        description: The friends was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", friends.updateRec);

/**
 * @swagger
 * /friends/{id}:
 *   delete:
 *     summary: Remove the friends by id
 *     tags: [Friends]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The friends id
 * 
 *     responses:
 *       200:
 *         description: The friends was deleted
 *       404:
 *         description: The friends was not found
 */

router.delete("/:id", friends.deleteRec);

module.exports = router;
