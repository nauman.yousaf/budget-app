const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const expenses = require('../controllers/controller.expenses');
/**

 * @swagger
 * components:
 *   schemas:
 *      Expenses:
 *       type: object
 *         - description
 *         - amount 
 *         - is_shared
 *         - category_id 
 *         - model_type 
 *         - model_id 
 *         - user_id 
 *         - budget_id 
 *         - division 
 *         - is_settled 
 *       properties:
 *         description:
 *           type: string
 *           description: Description of expense 
 *         amount:
 *           type: number
 *           description: Amount of expense         
 *         is_shared:
 *           type: boolean
 *           description: Expense is shared or not
 *         category_id:
 *           type: integer
 *           description: Expense category id 
 *         model_type:
 *           type: array
 *           items: {['individual', 'group', 'friend']}
 *           description: Model type 
 *         model_id:
 *           type: integer
 *           description: Model id
 *         user_id:
 *           type: integer
 *           description: User id
 *         budget_id:
 *           type: integer
 *           description: Budget id
 *         division:
 *           type: string
 *           description: Budget id
 *         is_settled:
 *           type: integer
 *           description: Settled or not
 *       example:
 *         description: Dinning at monal
 *         amount: 90.00
 *         is_shared: false
 *         category_id: 1
 *         model_type: group
 *         model_id: 1
 *         user_id: 3
 *         budget_id: 1
 *         division: { "squadName": "Super hero squad", "homeTown": "Metro City", "formed": 2016, "secretBase": "Super tower", "active": true, "members": [ { "name": "Molecule Man", "age": 29, "secretIdentity": "Dan Jukes", "powers": [ "Radiation resistance", "Turning tiny", "Radiation blast" ] }, { "name": "Madame Uppercut", "age": 39, "secretIdentity": "Jane Wilson", "powers": [ "Million tonne punch", "Damage resistance", "Superhuman reflexes" ] }, { "name": "Eternal Flame", "age": 1000000, "secretIdentity": "Unknown", "powers": [ "Immortality", "Heat Immunity", "Inferno", "Teleportation", "Interdimensional travel" ] } ] }
 *         is_settled: true
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Expenses
  *   description: The expenses managing API
  */

/**
 * @swagger
 * /expenses:
 *   get:
 *     summary: Returns the list of all the expenses
 *     tags: [Expenses]
 *     responses:
 *       200:
 *         description: The list of the expenses
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Expenses'
 */

router.get("/", expenses.getAll);

/**
 * @swagger
 * /expenses/{id}:
 *   get:
 *     summary: Get the Expense by id
 *     tags: [Expenses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Expense id
 *     responses:
 *       200:
 *         description: The Expense description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Expenses'
 *       404:
 *         description: The Expense was not found
 */

router.get("/:id", expenses.getRec);

/**
 * @swagger
 * /expenses:
 *   post:
 *     summary: Create a new Expense
 *     tags: [Expenses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Expenses'
 *     responses:
 *       200:
 *         description: The Expense was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Expenses'
 *       500:
 *         description: Some server error
 */

router.post("/", expenses.createRec);

/**
 * @swagger
 * /expenses/{id}:
 *  put:
 *    summary: Update the Expense by the id
 *    tags: [Expenses]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The Expense id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Expenses'
 *    responses:
 *      200:
 *        description: The Expense was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Expenses'
 *      404:
 *        description: The Expense was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", expenses.updateRec);

/**
 * @swagger
 * /expenses/{id}:
 *   delete:
 *     summary: Remove the Expense by id
 *     tags: [Expenses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Expense id
 * 
 *     responses:
 *       200:
 *         description: The Expense was deleted
 *       404:
 *         description: The Expense was not found
 */

router.delete("/:id", expenses.deleteRec);

module.exports = router;
