const express = require('express');
const router = express.Router();

router.get('/',(req,res)=>{

    res.send('Hello world');
});

router.get('/example',(req,res)=>{

    res.send('hittings example');
});

router.get('/example/:name/:age',(req,res)=>{

    console.log(req.query);
    res.send(req.params.name +":" + req.params.age );

});
router.get('/moods', (req, res, next) => {
  // Here we would send back the moods array in response
  res.send('hittings moods');
  next();
});

router.post('/hello',(req,res)=>{
    res.send('post req hits');
});

module.exports = router;
