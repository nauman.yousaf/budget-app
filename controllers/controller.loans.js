const db = require('../models/index');

const getAll = (req,res) => {
    db.Loans.findAll().then((data) => {
        if(!data){
            res.sendStatus(404)
        }
        res.send(data);
    });
}
const getRec =  (req,res) => {
    db.Loans.findByPk(req.params.id).then((data) => {
        if(!data){
            res.sendStatus(404)
        }
        res.send(data);
    });
}
const createRec = (req,res) => {

    try {
        const params = req.body;
        console.log(params);
        db.Loans.create({ 
            name: params.name,
            planned_expenses: params.planned_expenses,
            planned_incomes: params.planned_incomes,
            start_balance: params.start_balance,
            end_balance: params.end_balance,
            user_id: params.user_id,
            createdAt: db.sequelize.fn('NOW'),
            updatedAt: db.sequelize.fn('NOW')
            }).then((Budget)=>
            {
                console.log(Budget);
                res.send(Budget);
                next();
            });
            
	} catch (error) {
        console.log(error);
		return res.status(500).send(error);
	}

}

const updateRec = (req,res) => {

    try {
        const params = req.body;
        db.Loans.findByPk(req.params.id).then((data) => {
            console.log(data);
            if(!data){
                res.sendStatus(404)
            }
            data.update(
                {
                    name: params.name,
                    planned_expenses: params.planned_expenses,
                    planned_incomes: params.planned_incomes,
                    start_balance: params.start_balance,
                    end_balance: params.end_balance,
                    user_id: params.user_id,
                    updatedAt: db.sequelize.fn('NOW')
                },
              )
              res.status(200).send(data);
        })
	} catch (error) {
		return res.status(500).send(error);
	}

}

const deleteRec = (req,res,) => {

    try {
        const params = req.body;
        const id = req.params.id
        db.Loans.destroy({
            where:{id}
        }).then((data) => {
        console.log(data)
            if(!data){
                res.sendStatus(404)
            }
            res.status(200).send('The budget was deleted successfully');
        })
	} catch (error) {
		return res.status(500).send(error);
	}

}

module.exports = {
    getAll,
    getRec,
    updateRec,
    createRec,
    deleteRec
}
